package com.beanfun.javahiltexample.repository;

import androidx.lifecycle.LiveData;

import com.beanfun.javahiltexample.model.local.CafeDao;
import com.beanfun.javahiltexample.model.local.CafeEntity;
import com.beanfun.javahiltexample.model.remote.CafeApi;
import com.beanfun.javahiltexample.model.remote.CafeData;

import java.util.List;
import java.util.concurrent.ExecutorService;

import javax.inject.Inject;

import retrofit2.Callback;

public class CafeDataRepository {
    @Inject
    public ExecutorService databaseExecutor;
    @Inject
    public CafeDao cafeDao;
    @Inject
    public CafeApi cafeApi;

    @Inject
    public CafeDataRepository() {
    }

    public void fetchCafeData(String city, Callback<List<CafeData>> callback) {
        cafeApi.getCafeList(city).enqueue(callback);
    }

    public void insertCafeData(List<CafeEntity> cafeEntityList){
        databaseExecutor.execute(() -> cafeDao.insert(cafeEntityList));
    }

    public LiveData<List<CafeEntity>> getCafeList(){
        return cafeDao.getAll();
    }
}
