package com.beanfun.javahiltexample.di.module;

import com.beanfun.javahiltexample.model.remote.CafeApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(ApplicationComponent.class)
public class RemoteApiModule {
    public static final String CAFE_LIST = "/api/v1.2/cafes/{city}";
    @Provides
    @Singleton
    public static Retrofit provideRetrofit(){
        return new Retrofit.Builder()
                .baseUrl("https://cafenomad.tw")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    public static CafeApi provideCafeApi(Retrofit retrofit){
        return retrofit.create(CafeApi.class);
    }

}
