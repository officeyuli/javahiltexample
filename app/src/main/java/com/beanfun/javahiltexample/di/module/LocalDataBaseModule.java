package com.beanfun.javahiltexample.di.module;

import android.app.Application;

import androidx.room.Room;

import com.beanfun.javahiltexample.model.local.CafeDao;
import com.beanfun.javahiltexample.model.local.CafeDb;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;

@Module
@InstallIn(ApplicationComponent.class)
public class LocalDataBaseModule {
    private static final String DB_NAME = "cafe.db";

    @Provides
    @Singleton
    public static CafeDb providerCafeDb(Application application){
        return Room.databaseBuilder(application,CafeDb.class,DB_NAME)
                .build();
    }

    @Provides
    @Singleton
    public static CafeDao providePokeDao(CafeDb cafeDb){
        return cafeDb.getCafeDao();
    }

    @Provides
    public static ExecutorService databaseExecutor(){
        return Executors.newSingleThreadExecutor();
    }

}
