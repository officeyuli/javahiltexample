package com.beanfun.javahiltexample.model.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CafeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<CafeEntity> cafeList);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(CafeEntity cafeEntity);

    @Query("select * from Cafe limit 100")
    LiveData<List<CafeEntity>> getAll();


    @Query("select * from Cafe where star >= :starLevel order by star desc limit 100 ")
    List<CafeEntity> getHighStar(String starLevel);

    @Query("select * from Cafe where star >= :starLevel order by star desc limit 100")
    LiveData<List<CafeEntity>> getHighStarLiveList(String starLevel);
}
