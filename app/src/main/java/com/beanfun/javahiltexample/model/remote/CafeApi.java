package com.beanfun.javahiltexample.model.remote;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static com.beanfun.javahiltexample.di.module.RemoteApiModule.CAFE_LIST;

public interface CafeApi {
    @GET(CAFE_LIST)
    Call<List<CafeData>> getCafeList(@Path("city") String city);
}
