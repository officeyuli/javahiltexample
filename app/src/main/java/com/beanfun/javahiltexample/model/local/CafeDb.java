package com.beanfun.javahiltexample.model.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {CafeEntity.class},version = 1,exportSchema = false)
public abstract class CafeDb  extends RoomDatabase {
    public abstract CafeDao getCafeDao();
}
