package com.beanfun.javahiltexample.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.beanfun.javahiltexample.R;
import com.beanfun.javahiltexample.model.local.CafeEntity;
import com.beanfun.javahiltexample.model.remote.CafeData;
import com.beanfun.javahiltexample.viewmodel.MainViewModel;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainFragment extends Fragment {

    private MainViewModel mViewModel;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        initData();

    }

    private void initData() {
        mViewModel.fetchCafeData("taipei");
        mViewModel.getCafeListLiveData().observe(getViewLifecycleOwner(), cafeDataList -> {
            if (cafeDataList != null) {
                for (CafeEntity cafeEntity : cafeDataList) {
                    Log.e("yulitest", cafeEntity.toString());
                }
            }
        });
    }

}