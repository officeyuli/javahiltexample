package com.beanfun.javahiltexample.viewmodel;

import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.beanfun.javahiltexample.model.local.CafeEntity;
import com.beanfun.javahiltexample.model.remote.CafeData;
import com.beanfun.javahiltexample.usecase.CafeUseCase;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {

    private final CafeUseCase cafeUseCase;
    public LiveData<List<CafeEntity>> cafeListLiveData;

    @ViewModelInject
    public MainViewModel(CafeUseCase cafeUseCase) {
        this.cafeUseCase = cafeUseCase;
    }

    public LiveData<List<CafeEntity>> getCafeListLiveData() {
        if(cafeListLiveData ==null){
            cafeListLiveData = cafeUseCase.fetchCafeDataFromDb();
        }
        return cafeListLiveData;
    }

    public void fetchCafeData(String city) {
        cafeUseCase.fetchCafeDataFromServer(city, new Callback<List<CafeData>>() {
            @Override
            public void onResponse(@NonNull Call<List<CafeData>> call, Response<List<CafeData>> response) {
                if (response.body() != null) {
                    List<CafeEntity> cafeEntityList = new ArrayList<>();
                    for (CafeData cafeData : response.body()) {
                        CafeEntity cafeEntity = new CafeEntity();
                        cafeEntity.setId(cafeData.getId());
                        cafeEntity.setAddress(cafeData.getAddress());
                        cafeEntity.setName(cafeData.getName());
                        cafeEntity.setStar(cafeData.getTasty());
                        cafeEntityList.add(cafeEntity);
                    }
                    cafeUseCase.insertCafeData(cafeEntityList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<CafeData>> call, @NonNull Throwable t) {

            }
        });

    }
}