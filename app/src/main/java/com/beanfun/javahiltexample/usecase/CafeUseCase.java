package com.beanfun.javahiltexample.usecase;

import androidx.lifecycle.LiveData;

import com.beanfun.javahiltexample.model.local.CafeEntity;
import com.beanfun.javahiltexample.model.remote.CafeData;
import com.beanfun.javahiltexample.repository.CafeDataRepository;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Callback;

public class CafeUseCase {
    @Inject
    public CafeDataRepository cafeDataRepository;

    @Inject
    public CafeUseCase() {
    }

    public void fetchCafeDataFromServer(String city, Callback<List<CafeData>> callback) {
        cafeDataRepository.fetchCafeData(city, callback);
    }

    public LiveData<List<CafeEntity>> fetchCafeDataFromDb() {
        return cafeDataRepository.getCafeList();
    }

    public void insertCafeData(List<CafeEntity> cafeEntityList) {
        cafeDataRepository.insertCafeData(cafeEntityList);
    }

}
