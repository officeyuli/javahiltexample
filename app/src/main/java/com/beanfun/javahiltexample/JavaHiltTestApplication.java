package com.beanfun.javahiltexample;

import android.app.Application;
import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class JavaHiltTestApplication extends Application {
}
